<?php
/**
 * The header for our theme
 *
 * @package omik
 */

?>

<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    <meta property="og:title" content="">-->
    <!--    <meta property="og:type" content="">-->
    <!--    <meta property="og:url" content="">-->
    <!--    <meta property="og:image" content="">-->

    <link rel="apple-touch-icon" href="<?php echo get_theme_file_uri('img/icon.png') ?>">
    <link rel="icon" href="<?php echo get_theme_file_uri('img/icon.png') ?>">

    <meta name="theme-color" content="#102940">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page">
    <header>
        <?php /* TODO: the_custom_logo(); */ ?>
        <div class="title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
        <div class="subtitle"><?php echo get_bloginfo( 'description', 'display' ); ?></div>
    </header>

    <div id="skip-to-nav"><a href="#nav">⇊ <?php esc_html_e( 'Skip to navigation', '_s' ); ?></a></div>

    <div id="frame">
        <div id="article-container">

