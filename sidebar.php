<?php
/**
 * The sidebar containing the main widget area
 *
 * @package omik
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<!-- HERE -->

<nav id="nav">
    <section>
        <h2 class="nav-title">Menu</h2>

	    <?php wp_nav_menu(
		    array(
			    'theme_location' => 'menu-1',
			    'menu_id'        => 'primary-menu',
		    )
	    ); ?>
    </section>

	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</nav>
