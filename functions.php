<?php
/**
 * _s functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package _s
 */

if ( ! defined( 'OMIK_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'OMIK_VERSION', '1.0.0' );
}

 if ( ! function_exists( 'omik_setup' ) ) :
 	/**
 	 * Sets up theme defaults and registers support for various WordPress features.
 	 *
 	 * Note that this function is hooked into the after_setup_theme hook, which
 	 * runs before the init hook. The init hook is too late for some features, such
 	 * as indicating support for post thumbnails.
 	 */
 	function omik_setup() {
 		/*
 		 * Make theme available for translation.
 		 * Translations can be filed in the /languages/ directory.
 		 * If you're building a theme based on _s, use a find and replace
 		 * to change '_s' to the name of your theme in all the template files.
 		 */
 		load_theme_textdomain( 'omik', get_template_directory() . '/languages' );

 		// Add default posts and comments RSS feed links to head.
 		add_theme_support( 'automatic-feed-links' );

 		/*
 		 * Let WordPress manage the document title.
 		 * By adding theme support, we declare that this theme does not use a
 		 * hard-coded <title> tag in the document head, and expect WordPress to
 		 * provide it for us.
 		 */
 		add_theme_support( 'title-tag' );

 		/*
 		 * Enable support for Post Thumbnails on posts and pages.
 		 *
 		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 		 */
 		add_theme_support( 'post-thumbnails' );

 		// This theme uses wp_nav_menu() in one location.
 		register_nav_menus(
 			array(
 				'menu-1' => esc_html__( 'Primary', 'omik' ),
 			)
 		);

 		/*
 		 * Switch default core markup for search form, comment form, and comments
 		 * to output valid HTML5.
 		 */
 		add_theme_support(
 			'html5',
 			array(
 				'search-form',
 				'comment-form',
 				'comment-list',
 				'gallery',
 				'caption',
 				'style',
 				'script',
 			)
 		);

 		// Set up the WordPress core custom background feature.
// 		add_theme_support(
// 			'custom-background',
// 			apply_filters(
// 				'_s_custom_background_args',
// 				array(
// 					'default-color' => 'ffffff',
// 					'default-image' => '',
// 				)
// 			)
// 		);

 		// Add theme support for selective refresh for widgets.
 		add_theme_support( 'customize-selective-refresh-widgets' );

 		/**
 		 * Add support for core custom logo.
 		 *
 		 * @link https://codex.wordpress.org/Theme_Logo
 		 */
// 		add_theme_support(
// 			'custom-logo',
// 			array(
// 				'height'      => 250,
// 				'width'       => 250,
// 				'flex-width'  => true,
// 				'flex-height' => true,
// 			)
// 		);
 	}
 endif;
 add_action( 'after_setup_theme', 'omik_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
// function _s_content_width() {
// 	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
// }
// add_action( 'after_setup_theme', '_s_content_width', 0 );


/**
 * Add post class (single/multi) to
 */
function omik_add_post_class($classes) {
	if (is_page())
		return $classes;

	if (is_single()) {
		array_push($classes, 'article-single');
	} else {
		array_push($classes, 'article-multi');
	}

	return $classes;
}
add_filter('post_class', 'omik_add_post_class');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
 function omik_widgets_init() {
 	register_sidebar(
 		array(
 			'name'          => esc_html__( 'Sidebar', '_s' ),
 			'id'            => 'sidebar-1',
 			'description'   => esc_html__( 'Add widgets here.', '_s' ),
 			'before_widget' => '<section id="%1$s" class="widget %2$s">',
 			'after_widget'  => '</section>',
 			'before_title'  => '<h2 class="nav-title">',
 			'after_title'   => '</h2>',
 		)
 	);
 }
 add_action( 'widgets_init', 'omik_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function omik_scripts() {

    wp_enqueue_style( 'omik-normalize', get_template_directory_uri() . '/css/normalize.css', false, '8.0.1', 'all');
    wp_enqueue_style( 'omik-boilerplate', get_template_directory_uri() . '/css/boilerplate.css', false, '8.0.0', 'all');

	wp_enqueue_style( 'omik-style', get_stylesheet_uri(), array(), OMIK_VERSION );
	wp_style_add_data( '_s-style', 'rtl', 'replace' );

// TODO: remove
// 	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), OMIK_VERSION, true );

 	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
 		wp_enqueue_script( 'comment-reply' );
 	}
}
add_action( 'wp_enqueue_scripts', 'omik_scripts' );

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
 require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
// require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// if ( defined( 'JETPACK__VERSION' ) ) {
// 	require get_template_directory() . '/inc/jetpack.php';
// }
