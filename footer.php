<?php
/**
 * The template for displaying the footer
 *
 * @package omik
 */

?>
        </div>

        <?php get_sidebar(); ?>

    </div>
</div>

<footer>
    Copyright &copy; 2020 Rafal Macyszyn
    |
    <a href="<?php echo esc_url( __( 'https://wordpress.org/', '_s' ) ); ?>">
		<?php
		/* translators: %s: CMS name, i.e. WordPress. */
		printf( esc_html__( 'Proudly powered by %s', '_s' ), 'WordPress' );
		?>
    </a>
</footer>

<?php wp_footer(); ?>

</body>
</html>


